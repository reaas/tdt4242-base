"""Contains custom DRF permissions classes for the diets app
"""
from rest_framework import permissions
from diets.models import Diet


class IsOwner(permissions.BasePermission):
    """Checks whether the requesting user is also the owner of the existing object"""

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user


class IsAthlete(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsOwnerOfDiet(permissions.BasePermission):
    """Checks whether the requesting user is also the owner of the new or existing object"""

    def has_permission(self, request, view):
        if request.method == "POST":
            if request.data.get("diet"):
                diet_id = request.data["diet"].split("/")[-2]
                diet = Diet.objects.get(pk=diet_id)
                if diet:
                    return diet.owner == request.user
            return False

        return True

    def has_object_permission(self, request, view, obj):
        return obj.diet.owner == request.user


class IsCoachAndVisibleToCoach(permissions.BasePermission):
    """Checks whether the requesting user is the existing object's owner's coach
    and whether the object (diet) has a visibility of Public or Coach.
    """

    def has_object_permission(self, request, view, obj):
        return obj.owner.coach == request.user


class IsCoachOfDietAndVisibleToCoach(permissions.BasePermission):
    """Checks whether the requesting user is the existing diet's owner's coach
    and whether the object has a visibility of Public or Coach.
    """

    def has_object_permission(self, request, view, obj):
        return obj.diet.owner.coach == request.user


class IsPublic(permissions.BasePermission):
    """Checks whether the object (diet) has visibility of Public."""

    def has_object_permission(self, request, view, obj):
        return obj.visibility == "PU"


class IsDietPublic(permissions.BasePermission):
    """Checks whether the object's diet has visibility of Public."""

    def has_object_permission(self, request, view, obj):
        return obj.diet.visibility == "PU"


class IsReadOnly(permissions.BasePermission):
    """Checks whether the HTTP request verb is only for retrieving data (GET, HEAD, OPTIONS)"""

    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS


class IsCommentVisibleToUser(permissions.BasePermission):
    """
    Custom permission to only allow a comment to be viewed
    if one of the following holds:
    - The comment is on a public visibility workout
    - The comment was written by the user
    - The comment is on a coach visibility workout and the user is the workout owner's coach
    - The comment is on a workout owned by the user
    """

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner.
        return (
            obj.diet.visibility == "PU"
            or obj.owner == request.user
            or (obj.diet.visibility == "CO" and obj.owner.coach == request.user)
            or obj.diet.owner == request.user
        )