"""AppConfig for diets app
"""
from django.apps import AppConfig


class DietsConfig(AppConfig):
    """AppConfig for diets app

    Attributes:
        name (str): The name of the application
    """

    name = "diets"
