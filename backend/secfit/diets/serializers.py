"""Serializers for the diets application
"""
from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from diets.models import Diet, Mealtype, MealtypeInstance, DietFile, DietInstance, RememberMe
from users.models import User

class MealtypeInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an MealtypeInstance. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, mealtype, diet

    Attributes:
        diet:    The associated diet for this instance, represented by a hyperlink
    """

    diet = HyperlinkedRelatedField(
        queryset=Diet.objects.all(), view_name="diet-detail", required=False
    )

    class Meta:
        model = MealtypeInstance
        fields = ["url", "id", "mealtype", "diet"]


class DietInstanceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an DietInstance. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, diet, user

    Attributes:
        diet:    The associated diet for this instance, represented by a hyperlink
    """

    diet = HyperlinkedRelatedField(
        queryset=Diet.objects.all(), view_name="diet-detail", required=False
    )

    user = HyperlinkedRelatedField(
        queryset=User.objects.all(), view_name="user-detail", required=False
    )

    class Meta:
        model = DietInstance
        fields = ["url", "id", "diet", "user"]



class DietFileSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a DietFile. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, owner, file, diet

    Attributes:
        owner:      The owner (User) of the DietFile, represented by a username. ReadOnly
        diet:    The associate diet for this DietFile, represented by a hyperlink
    """

    owner = serializers.ReadOnlyField(source="owner.username")
    diet = HyperlinkedRelatedField(
        queryset=Diet.objects.all(), view_name="diet-detail", required=False
    )

    class Meta:
        model = DietFile
        fields = ["url", "id", "owner", "file", "diet"]

    def create(self, validated_data):
        return DietFile.objects.create(**validated_data)


class DietSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for a Diet. Hyperlinks are used for relationships by default.

    This serializer specifies nested serialization since a diet consists of DietFiles
    and MealtypeInstances.

    Serialized fields: url, id, name, date, notes, owner, owner_username, visiblity,
                       mealtype_instances, files

    Attributes:
        owner_username:     Username of the owning User
        mealtype_instance:  Serializer for ExericseInstances
        files:              Serializer for DietFiles
    """

    owner_username = serializers.SerializerMethodField()
    mealtype_instances = MealtypeInstanceSerializer(many=True, required=True)
    files = DietFileSerializer(many=True, required=False)

    class Meta:
        model = Diet
        fields = [
            "url",
            "id",
            "name",
            "date",
            "notes",
            "owner",
            "owner_username",
            "visibility",
            "mealtype_instances",
            "files",
        ]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating MealtypeInstances, DietFiles, and a Diet.

        This is needed to iterate over the files and mealtype instances, since this serializer is
        nested.

        Args:
            validated_data: Validated files and mealtype_instances

        Returns:
            Diet: A newly created Diet
        """
        mealtype_instances_data = validated_data.pop("mealtype_instances")
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        diet = Diet.objects.create(**validated_data)

        for mealtype_instance_data in mealtype_instances_data:
            MealtypeInstance.objects.create(diet=diet, **mealtype_instance_data)
        for file_data in files_data:
            DietFile.objects.create(
                diet=diet, owner=diet.owner, file=file_data.get("file")
            )

        return diet


    def clean_up_diet(self, instance, mealtype_instances_data, mealtype_instances):
        if len(mealtype_instances_data) > len(mealtype_instances.all()):
            for i in range(len(mealtype_instances.all()), len(mealtype_instances_data)):
                mealtype_instance_data = mealtype_instances_data[i]
                MealtypeInstance.objects.create(
                    diet=instance, **mealtype_instance_data
                )
        # Else if mealtype instances have been removed from the diet, then delete them
        elif len(mealtype_instances_data) < len(mealtype_instances.all()):
            for i in range(len(mealtype_instances_data), len(mealtype_instances.all())):
                mealtype_instances.all()[i].delete()


    def update(self, instance, validated_data):
        """Custom logic for updating a Diet with its MealtypeInstances and Diets.

        This is needed because each object in both mealtype_instances and files must be iterated
        over and handled individually.

        Args:
            instance (Diet): Current Diet object
            validated_data: Contains data for validated fields

        Returns:
            Diet: Updated Diet instance
        """
        mealtype_instances_data = validated_data.pop("mealtype_instances")
        mealtype_instances = instance.mealtype_instances

        instance.name = validated_data.get("name", instance.name)
        instance.notes = validated_data.get("notes", instance.notes)
        instance.visibility = validated_data.get("visibility", instance.visibility)
        instance.date = validated_data.get("date", instance.date)
        instance.save()

        # Handle MealtypeInstances

        # This updates existing mealtype instances without adding or deleting object.
        # zip() will yield n 2-tuples, where n is
        # min(len(mealtype_instance), len(mealtype_instance_data))
        for mealtype_instance, mealtype_instance_data in zip(
            mealtype_instances.all(), mealtype_instances_data
        ):
            mealtype_instance.mealtype = mealtype_instance_data.get(
                "mealtype", mealtype_instance.mealtype
            )
            mealtype_instance.save()


        self.clean_up_diet(instance, mealtype_instances_data, mealtype_instances)

        # Handle DietFiles

        if "files" in validated_data:
            files_data = validated_data.pop("files")
            files = instance.files

            for file, file_data in zip(files.all(), files_data):
                file.file = file_data.get("file", file.file)

            # If new files have been added, creating new DietFiles
            if len(files_data) > len(files.all()):
                for i in range(len(files.all()), len(files_data)):
                    DietFile.objects.create(
                        diet=instance,
                        owner=instance.owner,
                        file=files_data[i].get("file"),
                    )
            # Else if files have been removed, delete DietFiles
            elif len(files_data) < len(files.all()):
                for i in range(len(files_data), len(files.all())):
                    files.all()[i].delete()

        return instance


    def get_owner_username(self, obj):
        """Returns the owning user's username

        Args:
            obj (Diet): Current Diet

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class MealtypeSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an Mealtype. Hyperlinks are used for relationships by default.

    Serialized fields: url, id, name, description, instances

    Attributes:
        instances:  Associated mealtype instances with this Mealtype type. Hyperlinks.
    """

    instances = serializers.HyperlinkedRelatedField(
        many=True, view_name="mealtypeinstance-detail", read_only=True
    )

    class Meta:
        model = Mealtype
        fields = ["url", "id", "name", "description", "instances"]


class RememberMeSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for an RememberMe. Hyperlinks are used for relationships by default.

    Serialized fields: remember_me

    Attributes:
        remember_me:    Value of cookie used for remember me functionality
    """

    class Meta:
        model = RememberMe
        fields = ["remember_me"]
