"""Contains views for the diets application. These are mostly class-based views.
"""
from rest_framework import generics, mixins, permissions, filters

from rest_framework.parsers import (JSONParser)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.db.models import Q
from diets.parsers import MultipartJsonParser
from diets.permissions import (
    IsOwner,
    IsAthlete,
    IsCoachAndVisibleToCoach,
    IsOwnerOfDiet,
    IsCoachOfDietAndVisibleToCoach,
    IsReadOnly,
    IsPublic,
    IsDietPublic
)
from rest_framework.permissions import (AllowAny)
from diets.mixins import CreateListModelMixin
from diets.models import Diet, Mealtype, MealtypeInstance, DietFile, DietInstance
from diets.serializers import (
    DietSerializer,
    DietInstanceSerializer,
    MealtypeSerializer,
    RememberMeSerializer,
    MealtypeInstanceSerializer,
    DietFileSerializer
)
from django.core.exceptions import PermissionDenied
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.response import Response
import json
from collections import namedtuple
import base64
import pickle
from django.core.signing import Signer
from rest_framework.filters import OrderingFilter


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "users": reverse("user-list", request=request, format=format),
            "diets": reverse("diet-list", request=request, format=format),
            "mealtypes": reverse("mealtype-list", request=request, format=format),
            "mealtype-instances": reverse(
                "mealtype-instance-list", request=request, format=format
            ),
            "diet-files": reverse(
                "diet-file-list", request=request, format=format
            ),
            "diet-instances": reverse(
                "diet-instance-list", request=request, format=format
            ),
            "comments": reverse("comment-list", request=request, format=format),
            "likes": reverse("like-list", request=request, format=format),
        }
    )


# Allow users to save a persistent session in their browser
class RememberMe(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    serializer_class = RememberMeSerializer

    def get(self, request):
        if request.user.is_authenticated == False:
            raise PermissionDenied
        else:
            return Response({"remember_me": self.rememberme()})

    def post(self, request):
        cookie_object = namedtuple("Cookies", request.COOKIES.keys())(
            *request.COOKIES.values()
        )
        user = self.get_user(cookie_object)
        refresh = RefreshToken.for_user(user)
        return Response(
            {
                "refresh": str(refresh),
                "access": str(refresh.access_token),
            }
        )

    def get_user(self, cookie_object):
        decode = base64.b64decode(cookie_object.remember_me)
        user, sign = pickle.loads(decode)

        # Validate signature
        if sign == self.sign_user(user):
            return user

    def rememberme(self):
        creds = [self.request.user, self.sign_user(str(self.request.user))]
        return base64.b64encode(pickle.dumps(creds))

    def sign_user(self, username):
        signer = Signer()
        signed_user = signer.sign(username)
        return signed_user


class DietList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of a Diet, or displaying a list
    of Diets

    HTTP methods: GET, POST
    """

    serializer_class = DietSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]  # User must be authenticated to create/view diets
    parser_classes = [
        MultipartJsonParser,
        JSONParser,
    ]  # For parsing JSON and Multi-part requests
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["name", "date", "owner__username"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = Diet.objects.none()
        if self.request.user:
            qs = Diet.objects.filter(
                Q(visibility="PU")
                | (Q(visibility="CO") & Q(owner__coach=self.request.user))
            ).distinct()

        return qs


class DietDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the details of an individual Diet.

    HTTP methods: GET, PUT, DELETE
    """

    queryset = Diet.objects.all()
    serializer_class = DietSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (IsOwner | (IsReadOnly & (IsCoachAndVisibleToCoach | IsPublic)))
    ]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class MealtypeList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """Class defining the web response for the creation of an Mealtype, or
    a list of Mealtypes.

    HTTP methods: GET, POST
    """

    queryset = Mealtype.objects.all()
    serializer_class = MealtypeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class MealtypeDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the details of an individual Mealtype.

    HTTP methods: GET, PUT, PATCH, DELETE
    """

    queryset = Mealtype.objects.all()
    serializer_class = MealtypeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class MealtypeInstanceList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the creation"""

    serializer_class = MealtypeInstanceSerializer
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfDiet]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        qs = MealtypeInstance.objects.none()
        if self.request.user:
            qs = MealtypeInstance.objects.filter(
                Q(diet__owner=self.request.user)
                | (
                    (Q(diet__visibility="CO") | Q(diet__visibility="PU"))
                    & Q(diet__owner__coach=self.request.user)
                )
            ).distinct()

        return qs


class MealtypeInstanceDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    serializer_class = MealtypeInstanceSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (
            IsOwnerOfDiet
            | (IsReadOnly & (IsCoachOfDietAndVisibleToCoach | IsDietPublic))
        )
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class DietInstanceList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):
    """Class defining the web response for the creation"""

    serializer_class = DietInstanceSerializer
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfDiet]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        qs = DietInstance.objects.none()
        if self.request.user:
            qs = DietInstance.objects.filter(
                Q(diet__owner=self.request.user)
                | (
                    Q(diet__visibility="CO") | Q(diet__visibility="PU")
                )
            ).distinct()

        return qs


class DietInstanceDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    queryset = DietInstance.objects.all()
    serializer_class = DietInstanceSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (
            IsOwnerOfDiet
            | (IsReadOnly & (IsCoachOfDietAndVisibleToCoach | IsDietPublic))
        )
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class DietFileList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):

    queryset = DietFile.objects.all()
    serializer_class = DietFileSerializer
    permission_classes = [permissions.IsAuthenticated & IsOwnerOfDiet]
    parser_classes = [MultipartJsonParser, JSONParser]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = DietFile.objects.none()
        if self.request.user:
            qs = DietFile.objects.filter(
                Q(owner=self.request.user)
                | Q(diet__owner=self.request.user)
                | (
                    Q(diet__visibility="CO")
                    & Q(diet__owner__coach=self.request.user)
                )
            ).distinct()

        return qs


class DietFileDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    queryset = DietFile.objects.all()
    serializer_class = DietFileSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & (
            IsOwner
            | IsOwnerOfDiet
            | (IsReadOnly & (IsCoachOfDietAndVisibleToCoach | IsDietPublic))
        )
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
