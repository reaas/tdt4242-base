"""Contains the models for the diets Django application. Users
log diets (Diet), which contain instances (MealtypeInstance) of various
type of mealtypes (Mealtype). The user can also upload files (DietFile) .
"""
import os
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.auth import get_user_model
from users.models import User


class OverwriteStorage(FileSystemStorage):
    """Filesystem storage for overwriting files. Currently unused."""

    def get_available_name(self, name, max_length=None):
        """https://djangosnippets.org/snippets/976/
        Returns a filename that's free on the target storage system, and
        available for new content to be written to.

        Args:
            name (str): Name of the file
            max_length (int, optional): Maximum length of a file name. Defaults to None.
        """
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))


class Diet(models.Model):
    """Django model for a diet that users can log.

    A diet has several attributes, and is associated with one or more mealtypes
    (instances) and, optionally, files uploaded by the user.

    Attributes:
        name:        Name of the diet
        date:        Date the diet was performed or is planned
        notes:       Notes about the diet
        owner:       User that logged the diet
        visibility:  The visibility level of the diet: Public, Coach, or Private
    """

    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    notes = models.TextField()
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="diets"
    )

    # Visibility levels
    PUBLIC = "PU"  # Visible to all authenticated users
    COACH = "CO"  # Visible only to owner and their coach
    PRIVATE = "PR"  # Visible only to owner
    VISIBILITY_CHOICES = [
        (PUBLIC, "Public"),
        (COACH, "Coach"),
        (PRIVATE, "Private"),
    ]  # Choices for visibility level

    visibility = models.CharField(
        max_length=2, choices=VISIBILITY_CHOICES, default=COACH
    )

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return self.name


class Mealtype(models.Model):
    """Django model for an mealtype type that users can create.

    Each mealtype instance must have an mealtype type, e.g., Pushups, Crunches, or Lunges.

    Attributes:
        name:        Name of the mealtype type
        description: Description of the mealtype type
    """

    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.name


class MealtypeInstance(models.Model):
    """Django model for an instance of an mealtype.

    Each diet has one or more mealtype instances, each of a given type. For example,
    Kyle's diet on 15.06.2029 had one mealtype instance:  reps (unit) of
    10 () pushups (mealtype type)

    Attributes:
        diet:    The diet associated with this mealtype instance
        mealtype:   The mealtype type of this instance
    """

    diet = models.ForeignKey(
        Diet, on_delete=models.CASCADE, related_name="mealtype_instances"
    )
    mealtype = models.ForeignKey(
        Mealtype, on_delete=models.CASCADE, related_name="instances"
    )


def diet_directory_path(instance, filename):
    """Return path for which diet files should be uploaded on the web server

    Args:
        instance (DietFile): DietFile instance
        filename (str): Name of the file

    Returns:
        str: Path where diet file is stored
    """
    return f"diets/{instance.diet.id}/{filename}"


class DietInstance(models.Model):
    """Django model for an instance of a diet.

    Attributes:
        diet:    The diet associated with this mealtype instance
        user:   The mealtype type of this instance
    """

    diet = models.ForeignKey(
        Diet, on_delete=models.CASCADE, related_name="instances"
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="diet_instances"
    )


class DietFile(models.Model):
    """Django model for file associated with a diet. Basically a wrapper.

    Attributes:
        diet: The diet for which this file has been uploaded
        owner:   The user who uploaded the file
        file:    The actual file that's being uploaded
    """

    diet = models.ForeignKey(
        Diet, on_delete=models.CASCADE, related_name="files")
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="diet_files"
    )
    file = models.FileField(upload_to=diet_directory_path)


class RememberMe(models.Model):
    """Django model for an remember_me cookie used for remember me functionality.

    Attributes:
        remember_me:        Value of cookie used for remember me
    """

    remember_me = models.CharField(max_length=500)

    def __str__(self):
        return self.remember_me
