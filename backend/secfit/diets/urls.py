from django.urls import path, include
from diets import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = format_suffix_patterns(
    [
        path("", views.api_root),
        path("api/diets/", views.DietList.as_view(), name="diet-list"),
        path(
            "api/diets/<int:pk>/",
            views.DietDetail.as_view(),
            name="diet-detail",
        ),
        path("api/mealtypes/", views.MealtypeList.as_view(), name="mealtype-list"),
        path(
            "api/mealtypes/<int:pk>/",
            views.MealtypeDetail.as_view(),
            name="mealtype-detail",
        ),
        path(
            "api/mealtype-instances/",
            views.MealtypeInstanceList.as_view(),
            name="mealtype-instance-list",
        ),
        path(
            "api/mealtype-instances/<int:pk>/",
            views.MealtypeInstanceDetail.as_view(),
            name="mealtypeinstance-detail",
        ),
        path(
            "api/diet-instances/",
            views.DietInstanceList.as_view(),
            name="diet-instance-list",
        ),
        path(
            "api/diet-instances/<int:pk>/",
            views.DietInstanceDetail.as_view(),
            name="dietinstance-detail",
        ),
        path(
            "api/diet-files/",
            views.DietFileList.as_view(),
            name="diet-file-list",
        ),
        path(
            "api/diet-files/<int:pk>/",
            views.DietFileDetail.as_view(),
            name="dietfile-detail",
        ),
        path("", include("users.urls")),
        path("", include("comments.urls")),
        path("api/auth/", include("rest_framework.urls")),
        path("api/token/", TokenObtainPairView.as_view(),
             name="token_obtain_pair"),
        path("api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
        path("api/remember_me/", views.RememberMe.as_view(), name="remember_me"),
    ]
)
