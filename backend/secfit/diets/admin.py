"""Module for registering models from workouts app to admin page so that they appear
"""
from django.contrib import admin

from .models import Mealtype, MealtypeInstance, Diet, DietFile, DietInstance

admin.site.register(Mealtype)
admin.site.register(MealtypeInstance)
admin.site.register(DietInstance)
admin.site.register(Diet)
admin.site.register(DietFile)
