from django.test import TestCase
from users.serializers import UserSerializer
from users.models import User
from unittest.mock import MagicMock


# class TestPasswordValidationFailure(TestCase):
#     # This test will fail as there are no validators
#     # As such the except-section in validate_password is not covered as it is never reached

#     def get_initial(self):
#         x = {"password": '', "password1": 'testPassword'}
#         return x

#     def test_password_validation(self):
#         self.assertFalse(UserSerializer.validate_password(self, True))


class TestPasswordValidationSuccess(TestCase):

    def get_initial(self):
        x = {"password": 'testPassword', "password1": 'testPassword'}
        return x

    def test_password_validation(self):
        self.assertTrue(UserSerializer.validate_password(self, True))


class TestCreate(TestCase):

    def test_create_user_is_the_same(self):
        self.data = {"username": 'Peer Gynt', "email": 'dulyver@gmail.com', "password": '1234',
                     "phone_number": 10101010, "country": 'Norge', "city": 'Bergen', "street_address": 'Den ene vegen som fantes på den tiden'}
        serialize = UserSerializer.create(self, self.data)
        users = User.objects.all()
        self.assertEqual(serialize, users[0])
