const generateStringWithLengthX = (x) => {
  let longString = '';
  for(let i = 0; i < x; i++) {
    longString += 'a';
  }

  return longString;
}

const randomUsername = () => {
  return Math.random().toString(36).substring(7);
}

describe('boundary tests workout page', () => {
  before(() => {
    cy.visit('../../www/register.html');

    username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('#btn-create-account')
      .click()
      .wait(1000)
      .then(() => {
        cy.get('#navbarNavAltMarkup')
          .get('#nav-exercises')
          .click()
          .then(() => {
            cy.get('#btn-create-exercise')
              .click()
              .then(() => {
                cy.get('#inputName')
                  .type(generateStringWithLengthX(10));

                cy.get('#inputDescription')
                  .type(generateStringWithLengthX(10));
                
                cy.get('#inputUnit')
                  .type('10');

                cy.get('#btn-ok-exercise')
                  .click();
              });
          });
      });
  });

  let username = '';
  beforeEach(() => {
    cy.visit('../../www/register.html');

    username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('#btn-create-account')
      .click()
      .then(() => {
        cy.get('#btn-create-workout')
          .click();
      });
  });

  it('alert should be visible if name is longer than 100 characters', () => {
    cy.get('#inputName')
      .type(generateStringWithLengthX(109));

    cy.get('#inputDateTime')
      .type('2021-03-11T10:00')

    cy.get('#inputNotes')
      .type(generateStringWithLengthX(10));

    cy.get('#btn-ok-workout')
      .click();

    cy.get('.alert')
      .should('be.visible');
  });

  it('alert should be visible if name is empty', () => {
    cy.get('#inputDateTime')
      .type('2021-03-11T10:00')

    cy.get('#inputNotes')
      .type(generateStringWithLengthX(10));

    cy.get('#btn-ok-workout')
      .click();

    cy.get('.alert')
      .should('be.visible');
  });

  it('owner should be readonly', () => {
    cy.get('#inputOwner')
      .should('have.attr', 'readonly')
  });

  it('owner should equal current username', () => {
    cy.get('#inputOwner')
      .should('have.value', username);
  });

  it('visibility should have three options with pre-defined values', () => {
    cy.get('[name="visibility"]')
      .find('option')
      .should('have.length', 3);

    cy.get('[name="visibility"]')
      .select('Public')
      .should('have.value', 'PU');

    cy.get('[name="visibility"]')
      .select('Coach')
      .should('have.value', 'CO');

    cy.get('[name="visibility"]')
      .select('Private')
      .should('have.value', 'PR');
  });

  it('alert should be visible if notes are empty', () => {
    cy.get('#inputName')
      .type(generateStringWithLengthX(10));

    cy.get('#inputDateTime')
      .type('2021-03-11T10:00')

    cy.get('#btn-ok-workout')
      .click();

    cy.get('.alert')
      .should('be.visible');
  });
});
