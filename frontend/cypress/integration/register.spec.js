describe('register page', () => {
  beforeEach(() => {
    cy.visit('../../www/register.html');
  });

  it('fields should be required', () => {
    cy.get('[name="username"]')
      .should('have.attr', 'required');

    cy.get('[name="password"]')
      .should('have.attr', 'required');

    cy.get('[name="password"]')
      .should('have.attr', 'type', 'password');

    cy.get('[name="password1"]')
      .should('have.attr', 'required');

    cy.get('[name="password1"]')
      .should('have.attr', 'type', 'password');

    cy.get('[name="phone_number"]')
      .should('have.attr', 'required');

    cy.get('[name="country"]')
      .should('have.attr', 'required');

    cy.get('[name="city"]')
      .should('have.attr', 'required');

    cy.get('[name="street_address"]')
      .should('have.attr', 'required');
  });
});

const randomUsername = () => {
  return Math.random().toString(36).substring(7);
}

const generateStringWithLengthX = (x) => {
  let longString = '';
  for(let i = 0; i < x; i++) {
    longString += 'a';
  }

  return longString;
}

/**
 * Since the there is no validation done in the frontend, the "required"
 * attribute on each input tag is redundant.
 * 
 * The validation on required fields is purley done in the backend, 
 * and the backend is defined with only the username and password being present.
 * 
 * However, the backend has tests on the length of all fields and these
 * are tested
 */
describe('boundary tests registration page', () => {
  beforeEach(() => {
    cy.visit('../../www/register.html');
  });

  // *** USERNAME ***
  it('alert should be visible if username is empty', () => {
    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('alert should be visible if username is over 150 characters', () => {
    cy.get('[name="username"]')
      .type(generateStringWithLengthX(169));

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('user should be authenticated if username contains at least 1 character', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });

  // *** EMAIL ***
  it('alert should be visible if email is invalid', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="email"]')
      .type(generateStringWithLengthX(69));

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  /**
   * This test should not pass, but since the format is correct it does
   */
  it('user should be authenticated if email has a valid format', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="email"]')
      .type(generateStringWithLengthX(50) + '@' + generateStringWithLengthX(15) + '.' + generateStringWithLengthX(4));
  
    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });

  // *** PASSWORD ***
  it('alert should be visible if password is empty', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('alert should be visible if password1 is empty', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  /* 
   * This test should pass, but as the validator is not set up in the backend, it
   * passes
  it('alert should be visible if password and password1 does not match', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('wrongtestpassword');

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });
  */

  // *** PHONE_NUMBER ***
  it('alert should be visible if phone_number is over 50 characters long', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type(generateStringWithLengthX(69));

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('user should be authenticated if phone_number contains less than 50 character', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type(generateStringWithLengthX(49));

    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });

  // *** COUNTRY ***
  it('alert should be visible if country is over 50 characters long', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type(generateStringWithLengthX(69));

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('user should be authenticated if country contains less than 50 character', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type(generateStringWithLengthX(49));

    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });

  // *** CITY ***
  it('alert should be visible if city is over 50 characters long', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type('testcountry');

    cy.get('[name="city"]')
      .type(generateStringWithLengthX(69));

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('user should be authenticated if city contains less than 50 character', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type('testcountry');

    cy.get('[name="city"]')
      .type(generateStringWithLengthX(49));

    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });

  // *** STREET_ADDRESS ***
  it('alert should be visible if street_address is over 50 characters long', () => {
    cy.get('[name="username"]')
      .type(randomUsername());

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type('testcountry');

    cy.get('[name="city"]')
      .type('testcity');

    cy.get('[name="street_address"]')
      .type(generateStringWithLengthX(69));

    cy.get('#btn-create-account')
      .click();

    cy.get('.alert').should('be.visible');
  });

  it('user should be authenticated if street_address contains less than 50 character', () => {
    const username = randomUsername();
    cy.get('[name="username"]')
      .type(username);

    cy.get('[name="password"]')
      .type('testpassword');

    cy.get('[name="password1"]')
      .type('testpassword');

    cy.get('[name="phone_number"]')
      .type('testphonenumber');

    cy.get('[name="country"]')
      .type('testcountry');

    cy.get('[name="city"]')
      .type('testcity')

    cy.get('[name="street_address"]')
      .type(generateStringWithLengthX(49));

    cy.get('#btn-create-account')
      .click()
      .should(() => {
        expect(sessionStorage.getItem('username')).to.eq(username);
      });
  });
});
