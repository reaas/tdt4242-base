let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;


function handleCancelButtonDuringEdit() {
    hideButtons();

    let form = document.querySelector("#form-mealtype");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");

    oldFormData.delete("name");
    oldFormData.delete("description");

}

function handleCancelButtonDuringCreate() {
    window.location.replace("diet.html");
}

async function createMealtype() {
    let form = document.querySelector("#form-mealtype");
    let formData = new FormData(form);
    let body = {
        "name": formData.get("name"),
        "description": formData.get("description"),
    };

    let response = await sendRequest("POST", `${HOST}/api/mealtypes/`, body);

    if (response.ok) {
        window.location.replace("diet.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new mealtype!", data);
        document.body.prepend(alert);
    }
}

function handleEditMealtypeButtonClick() {
    setReadOnly(false, "#form-mealtype");

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-mealtype");
    oldFormData = new FormData(form);
}

async function deleteMealtype(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/mealtypes/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete mealtype ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("mealtypes.html");
    }
}

async function retrieveMealtype(id) {
    let response = await sendRequest("GET", `${HOST}/api/mealtypes/${id}/`);
    console.log(response.ok);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve mealtype data!", data);
        document.body.prepend(alert);
    } else {
        let mealtypeData = await response.json();
        let form = document.querySelector("#form-mealtype");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = mealtypeData[key];
            input.value = newVal;
        }
    }
}

async function updateMealtype(id) {
    let form = document.querySelector("#form-mealtype");
    let formData = new FormData(form);
    let body = {
        "name": formData.get("name"),
        "description": formData.get("description"),
    };
    let response = await sendRequest("PUT", `${HOST}/api/mealtypes/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update mealtype ${id}`, data);
        document.body.prepend(alert);
    } else {
        hideButtons();

        oldFormData.delete("name");
        oldFormData.delete("description");
    }
}

function hideButtons() {
    setReadOnly(true, "#form-mealtype");
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-mealtype");
    okButton = document.querySelector("#btn-ok-mealtype");
    deleteButton = document.querySelector("#btn-delete-mealtype");
    editButton = document.querySelector("#btn-edit-mealtype");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const mealtypeId = urlParams.get('id');
        await retrieveMealtype(mealtypeId);

        editButton.addEventListener("click", handleEditMealtypeButtonClick);
        deleteButton.addEventListener("click", (async (id) => deleteMealtype(id)).bind(undefined, mealtypeId));
        okButton.addEventListener("click", (async (id) => updateMealtype(id)).bind(undefined, mealtypeId));
    }
    //create
    else {
        setReadOnly(false, "#form-mealtype");

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => createMealtype());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});