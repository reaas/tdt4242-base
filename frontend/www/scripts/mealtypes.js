async function fetchMealtypes(request) {
    let response = await sendRequest("GET", `${HOST}/api/mealtypes/`);

    if (response.ok) {
        let data = await response.json();

        let mealtypes = data.results;
        let container = document.getElementById('div-content');
        let mealtypeTemplate = document.querySelector("#template-mealtype");
        mealtypes.forEach(mealtype => {
            const mealtypeAnchor = mealtypeTemplate.content.firstElementChild.cloneNode(true);
            mealtypeAnchor.href = `mealtype.html?id=${mealtype.id}`;

            const h5 = mealtypeAnchor.querySelector("h5");
            h5.textContent = mealtype.name;

            const p = mealtypeAnchor.querySelector("p");
            p.textContent = mealtype.description;   

            container.appendChild(mealtypeAnchor);
        });
    }

    return response;
}

function createMealtype() {
    window.location.replace("mealtype.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-mealtype");
    createButton.addEventListener("click", createMealtype);

    let response = await fetchMealtypes();
    
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve mealtypes!", data);
        document.body.prepend(alert);
    }
});