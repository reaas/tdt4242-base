async function fetchDiets(ordering) {
    let response = await sendRequest("GET", `${HOST}/api/diets/?ordering=${ordering}`);

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        let data = await response.json();

        let diets = data.results;
        let container = document.getElementById('div-content');
        diets.forEach(diet => {
            let templateDiet = document.querySelector("#template-diet");
            let cloneDiet = templateDiet.content.cloneNode(true);

            let aDiet = cloneDiet.querySelector("a");
            aDiet.href = `diet.html?id=${diet.id}`;

            let h5 = aDiet.querySelector("h5");
            h5.textContent = diet.name;

            let localDate = new Date(diet.date);

            let table = aDiet.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[0].textContent = localDate.toLocaleDateString(); // Date
            rows[1].querySelectorAll("td")[0].textContent = localDate.toLocaleTimeString(); // Time
            rows[2].querySelectorAll("td")[0].textContent = diet.owner_username; //Owner
            rows[3].querySelectorAll("td")[0].textContent = diet.mealtype_instances.length; // Mealtypes

            container.appendChild(aDiet);
        });
        return diets;
    }
}

function createDiet() {
    window.location.replace("diet.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-diet");
    createButton.addEventListener("click", createDiet);
    let ordering = "-date";

    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('ordering')) {
        let aSort = null;
        ordering = urlParams.get('ordering');
        if (ordering == "name" || ordering == "owner" || ordering == "date") {
            aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
            aSort.href = `?ordering=-${ordering}`;
        }
    }

    let currentSort = document.querySelector("#current-sort");
    currentSort.innerHTML = (ordering.startsWith("-") ? "Descending" : "Ascending") + " " + ordering.replace("-", "");

    let currentUser = await getCurrentUser();
    // grab username
    if (ordering.includes("owner")) {
        ordering += "__username";
    }
    let diets = await fetchDiets(ordering);

    let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
    for (let tabEl of tabEls) {
        tabEl.addEventListener('show.bs.tab', function (event) {
            let dietAnchors = document.querySelectorAll('.diet');
            for (let j = 0; j < diets.length; j++) {
                let diet = diets[j];
                let dietAnchor = dietAnchors[j];

                add_or_remove_diet_anchor(event.currentTarget, currentUser, dietAnchor, diet);
            }
        });
    }
});

function add_or_remove_diet_anchor(currentTarget, currentUser, dietAnchor, diet) {
    switch (currentTarget.id) {
        case "list-my-diets-list":
            if (diet.owner == currentUser.url) {
                dietAnchor.classList.remove('hide');
            } else {
                dietAnchor.classList.add('hide');
            }
            break;
        case "list-athlete-diets-list":
            if (currentUser.athletes && currentUser.athletes.includes(diet.owner)) {
                dietAnchor.classList.remove('hide');
            } else {
                dietAnchor.classList.add('hide');
            }
            break;
        case "list-public-diets-list":
            if (diet.visibility == "PU") {
                dietAnchor.classList.remove('hide');
            } else {
                dietAnchor.classList.add('hide');
            }
            break;
        default:
            dietAnchor.classList.remove('hide');
            break;
    }
}