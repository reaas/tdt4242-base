let cancelDietButton;
let okDietButton;
let deleteDietButton;
let editDietButton;
let postCommentButton;

async function retrieveDiet(id) {
    let dietData = null;
    let response = await sendRequest("GET", `${HOST}/api/diets/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve diet data!", data);
        document.body.prepend(alert);
    } else {
        dietData = await response.json();
        let form = document.querySelector("#form-diet");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let formInput = form.querySelector(selector);
            let newVal = dietData[key];
            if (key == "date") {
                // Creating a valid datetime-local string with the correct local time
                let date = new Date(newVal);
                date = new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000)).toISOString(); // get ISO format for local time
                newVal = date.substring(0, newVal.length - 1);    // remove Z (since this is a local time, not UTC)
            }
            if (key != "files") {
                formInput.value = newVal;
            }
        }

        let input = form.querySelector("select:disabled");
        input.value = dietData["visibility"];
        let filesDiv = document.querySelector("#uploaded-files");
        for (let file of dietData.files) {
            let a = document.createElement("a");
            a.href = file.file;
            let pathArray = file.file.split("/");
            a.text = pathArray[pathArray.length - 1];
            a.className = "me-2";
            filesDiv.appendChild(a);
        }

        // fetch mealtype types
        let mealtypeTypeResponse = await sendRequest("GET", `${HOST}/api/mealtypes/`);
        let mealtypeTypes = await mealtypeTypeResponse.json();

        addMealtypeOptions(mealtypeTypes, dietData);

        return dietData;
    }
}

    function addMealtypeOptions(mealtypeTypes, dietData) {
        for (let i = 0; i < dietData.mealtype_instances.length; i++) {
            let templateMealtype = document.querySelector("#template-mealtype");
            let divMealtypeContainer = templateMealtype.content.firstElementChild.cloneNode(true);

            let mealtypeTypeLabel = divMealtypeContainer.querySelector('.mealtype-type');
            mealtypeTypeLabel.for = `inputMealtypeType${i}`;

            let mealtypeTypeSelect = divMealtypeContainer.querySelector("select");
            mealtypeTypeSelect.id = `inputMealtypeType${i}`;
            mealtypeTypeSelect.disabled = true;

            let splitUrl = dietData.mealtype_instances[i].mealtype.split("/");
            let currentMealtypeTypeId = splitUrl[splitUrl.length - 2];
            let currentMealtypeType = "";

            for (let j = 0; j < mealtypeTypes.count; j++) {
                let option = document.createElement("option");
                option.value = mealtypeTypes.results[j].id;
                if (currentMealtypeTypeId == mealtypeTypes.results[j].id) {
                    currentMealtypeType = mealtypeTypes.results[j];
                }
                option.innerText = mealtypeTypes.results[j].name;
                mealtypeTypeSelect.append(option);
            }

            mealtypeTypeSelect.value = currentMealtypeType.id;

            let mealtypesDiv = document.querySelector("#div-mealtypes");
            mealtypesDiv.appendChild(divMealtypeContainer);
        }
    }

    function handleCancelDuringDietEdit() {
        location.reload();
    }

    function handleEditDietButtonClick() {
        let addMealtypeButton = document.querySelector("#btn-add-mealtype");
        let removeMealtypeButton = document.querySelector("#btn-remove-mealtype");
        let addNewMealtypeButton = document.querySelector("#btn-add-new-mealtype");




        setReadOnly(false, "#form-diet");
        document.querySelector("#inputOwner").readOnly = true;  // owner field should still be readonly 

        editDietButton.className += " hide";
        okDietButton.className = okDietButton.className.replace(" hide", "");
        cancelDietButton.className = cancelDietButton.className.replace(" hide", "");
        deleteDietButton.className = deleteDietButton.className.replace(" hide", "");
        addMealtypeButton.className = addMealtypeButton.className.replace(" hide", "");
        removeMealtypeButton.className = removeMealtypeButton.className.replace(" hide", "");
        addNewMealtypeButton.className = addNewMealtypeButton.className.replace(" hide", "");

        cancelDietButton.addEventListener("click", handleCancelDuringDietEdit);

    }

    async function deleteDiet(id) {
        let response = await sendRequest("DELETE", `${HOST}/api/diets/${id}/`);
        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert(`Could not delete diet ${id}!`, data);
            document.body.prepend(alert);
        } else {
            window.location.replace("diets.html");
        }
    }

    async function updateDiet(id) {
        let submitForm = generateDietForm();

        let response = await sendRequest("PUT", `${HOST}/api/diets/${id}/`, submitForm, "");
        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert("Could not update diet!", data);
            document.body.prepend(alert);
        } else {
            location.reload();
        }
    }

    function generateDietForm() {
        let form = document.querySelector("#form-diet");

        let formData = new FormData(form);
        let submitForm = new FormData();

        submitForm.append("name", formData.get('name'));
        let date = new Date(formData.get('date')).toISOString();
        submitForm.append("date", date);
        submitForm.append("notes", formData.get("notes"));
        submitForm.append("visibility", formData.get("visibility"));

        // adding mealtype instances
        let mealtypeInstances = [];
        let mealtypeInstancesTypes = formData.getAll("type");
        for (let value of mealtypeInstancesTypes) {
            mealtypeInstances.push({
                mealtype: `${HOST}/api/mealtypes/${value}/`,
            });
        }

        submitForm.append("mealtype_instances", JSON.stringify(mealtypeInstances));
        // adding files
        for (let file of formData.getAll("files")) {
            submitForm.append("files", file);
        }
        return submitForm;
    }

    async function createDiet() {
        let submitForm = generateDietForm();

        let response = await sendRequest("POST", `${HOST}/api/diets/`, submitForm, "");

        if (response.ok) {
            window.location.replace("diets.html");
        } else {
            let data = await response.json();
            let alert = createAlert("Could not create new diet!", data);
            document.body.prepend(alert);
        }
    }

    function handleCancelDuringDietCreate() {
        window.location.replace("diets.html");
    }

    function createNewMealtype() {
        window.location.replace("mealtype.html")
    }

    async function createBlankMealtype() {

        let mealtypeTypeResponse = await sendRequest("GET", `${HOST}/api/mealtypes/`);
        let mealtypeTypes = await mealtypeTypeResponse.json();

        let mealtypeTemplate = document.querySelector("#template-mealtype");
        let divMealtypeContainer = mealtypeTemplate.content.firstElementChild.cloneNode(true);
        let mealtypeTypeSelect = divMealtypeContainer.querySelector("select");

        for (let i = 0; i < mealtypeTypes.count; i++) {
            let option = document.createElement("option");
            option.value = mealtypeTypes.results[i].id;
            option.innerText = mealtypeTypes.results[i].name;
            mealtypeTypeSelect.append(option);
        }

        let currentMealtypeType = mealtypeTypes.results[0];
        mealtypeTypeSelect.value = currentMealtypeType.name;

        let divMealtypes = document.querySelector("#div-mealtypes");
        divMealtypes.appendChild(divMealtypeContainer);
    }

    function removeMealtype(event) {
        let divMealtypeContainers = document.querySelectorAll(".div-mealtype-container");
        if (divMealtypeContainers && divMealtypeContainers.length > 0) {
            divMealtypeContainers[divMealtypeContainers.length - 1].remove();
        }
    }

    function addComment(author, text, date, append) {
        /* Taken from https://www.bootdey.com/snippets/view/Simple-Comment-panel#css*/
        let commentList = document.querySelector("#comment-list");
        let listElement = document.createElement("li");
        listElement.className = "media";
        let commentBody = document.createElement("div");
        commentBody.className = "media-body";
        let dateSpan = document.createElement("span");
        dateSpan.className = "text-muted pull-right me-1";
        let smallText = document.createElement("small");
        smallText.className = "text-muted";

        if (date != "Now") {
            let localDate = new Date(date);
            smallText.innerText = localDate.toLocaleString();
        } else {
            smallText.innerText = date;
        }

        dateSpan.appendChild(smallText);
        commentBody.appendChild(dateSpan);

        let strong = document.createElement("strong");
        strong.className = "text-success";
        strong.innerText = author;
        commentBody.appendChild(strong);
        let p = document.createElement("p");
        p.innerHTML = text;

        commentBody.appendChild(strong);
        commentBody.appendChild(p);
        listElement.appendChild(commentBody);

        if (append) {
            commentList.append(listElement);
        } else {
            commentList.prepend(listElement);
        }

    }

    async function createComment(dietid) {
        let commentArea = document.querySelector("#comment-area");
        let content = commentArea.value;
        let body = { diet: `${HOST}/api/diets/${dietid}/`, content: content };

        let response = await sendRequest("POST", `${HOST}/api/diets/comments/`, body);
        if (response.ok) {
            addComment(sessionStorage.getItem("username"), content, "Now", false);
        } else {
            let data = await response.json();
            let alert = createAlert("Failed to create comment!", data);
            document.body.prepend(alert);
        }
    }

    async function retrieveComments(dietid) {
        let response = await sendRequest("GET", `${HOST}/api/diets/comments/`);
        if (!response.ok) {
            let data = await response.json();
            let alert = createAlert("Could not retrieve comments!", data);
            document.body.prepend(alert);
        } else {
            let data = await response.json();
            let comments = data.results;
            for (let comment of comments) {
                let splitArray = comment.diet.split("/");
                if (splitArray[splitArray.length - 2] == dietid) {
                    addComment(comment.owner, comment.content, comment.timestamp, true);
                }
            }
        }
    }

    window.addEventListener("DOMContentLoaded", async () => {
        cancelDietButton = document.querySelector("#btn-cancel-diet");
        okDietButton = document.querySelector("#btn-ok-diet");
        deleteDietButton = document.querySelector("#btn-delete-diet");
        editDietButton = document.querySelector("#btn-edit-diet");
        postCommentButton = document.querySelector("#post-comment");
        let divCommentRow = document.querySelector("#div-comment-row");
        let buttonAddMealtype = document.querySelector("#btn-add-mealtype");
        let buttonRemoveMealtype = document.querySelector("#btn-remove-mealtype");
        let buttonAddNewMealtype = document.querySelector("#btn-add-new-mealtype");

        buttonAddMealtype.addEventListener("click", createBlankMealtype);
        buttonRemoveMealtype.addEventListener("click", removeMealtype);
        buttonAddNewMealtype.addEventListener("click", createNewMealtype)

        const urlParams = new URLSearchParams(window.location.search);
        let currentUser = await getCurrentUser();

        if (urlParams.has('id')) {
            const id = urlParams.get('id');
            let dietData = await retrieveDiet(id);
            await retrieveComments(id);

            if (dietData["owner"] == currentUser.url) {
                editDietButton.classList.remove("hide");
                editDietButton.addEventListener("click", handleEditDietButtonClick);
                deleteDietButton.addEventListener("click", (async (delId) => deleteDiet(delId)).bind(undefined, delId));
                okDietButton.addEventListener("click", (async (okId) => updateDiet(okId)).bind(undefined, okId));
                postCommentButton.addEventListener("click", (async (postId) => createComment(postId)).bind(undefined, postId));
                divCommentRow.className = divCommentRow.className.replace(" hide", "");
            } else {
                postCommentButton.addEventListener("click", (async (postId) => createComment(postId)).bind(undefined, postId));
            }
        } else {
            await createBlankMealtype();
            let ownerInput = document.querySelector("#inputOwner");
            ownerInput.value = currentUser.username;
            setReadOnly(false, "#form-diet");
            ownerInput.readOnly = !ownerInput.readOnly;

            okDietButton.className = okDietButton.className.replace(" hide", "");
            cancelDietButton.className = cancelDietButton.className.replace(" hide", "");
            buttonAddMealtype.className = buttonAddMealtype.className.replace(" hide", "");
            buttonRemoveMealtype.className = buttonRemoveMealtype.className.replace(" hide", "");

            okDietButton.addEventListener("click", async () => createDiet());
            cancelDietButton.addEventListener("click", handleCancelDuringDietCreate);
            divCommentRow.className += " hide";
        }

    });